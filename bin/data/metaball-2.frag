#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_mouse;
uniform vec2 u_resolution;

void main( ) {
 
 vec2 position = ( gl_FragCoord.xy / u_resolution.xy );
 
 position.y = 1.0 - position.y;
 
 position.x -= 0.5;
 position.y -= 0.5;
 
 float scale = 5.0;
 
 position *= scale;
 
 
 vec2 i =  position + vec2(cos(position.x-u_time) + sin(position.x-u_time), sin(position.y - u_time) + cos(position.x-u_time));
 //values for position.x / position.y are different for each pixel 
 float luma = length(i);
 //make a single colour :
 //luma = floor(luma);
 
 //stripey luma 
 //luma *= 50.0;
 luma = mod(luma,1.0);

 
 gl_FragColor = vec4(luma);

}