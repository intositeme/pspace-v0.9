#version 150

#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
uniform float circ_opacity;
uniform vec2 u_circleDimension;
out vec4 outputColor;

// Reference to
// http://thndl.com/square-shaped-shaders.html



float drawCircleRings (vec2 pixel, vec2 center, float radius, int count) {
  float spacing = 0.02;
  float dist = length(center - pixel)  ;
  float antiAlias = 2./radius;
  float thickness = 1./float(count) -spacing;
  float t = 0.;
  for(int i=0;i<count;++i)
  {
    //result += uLightsPos[i];
    float myThickness = thickness *  fract (abs ( sin(u_time*2. + float(i)/6. ) *  (cos(u_time/4.)) ) );
    float sI = max (thickness * float(i) , 0. ) + (spacing* float(i) );

    t += smoothstep ( sI - antiAlias, sI , dist / (radius)) - smoothstep ( sI+myThickness, sI+ myThickness + antiAlias, dist / (radius)) ;
  }
    return t;
}

void main(){
  
  vec3 color1 = vec3(0.8, 0.8, 0.8);
  vec3 color = vec3 (0.);
  vec2 pos = u_mouse.xy;
  pos.y = u_resolution.y - pos.y;
  float pct3 =  drawCircleRings(gl_FragCoord.xy, pos, u_circleDimension.x , 40);
  color = vec3 ( color1*pct3) ;


  // outputColor = vec4(color* circ_opacity, 0.3 );
  outputColor = vec4(color* circ_opacity, pct3 * circ_opacity* 0.5 );
}