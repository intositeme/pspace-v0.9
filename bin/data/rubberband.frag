
#version 150

#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
uniform float circ_opacity;
uniform vec2 u_circleDimension;
out vec4 outputColor;


vec2 random2(vec2 st){
    st = vec2( dot(st,vec2(127.1,311.7)),
              dot(st,vec2(269.5,183.3)) );
    return -1.0 + 2.0*fract(sin(st)*43758.5453123);
}

// Value Noise by Inigo Quilez - iq/2013
// https://www.shadertoy.com/view/lsf3WH
float noise(vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    vec2 u = f*f*(3.0-2.0*f);

    return mix( mix( dot( random2(i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ), 
                     dot( random2(i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
                mix( dot( random2(i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ), 
                     dot( random2(i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

float shapePosition(vec2 st, vec2 pos, float radius) {
    st = pos-st;
    float r = length(st)*2.0;
    float a = atan(st.y,st.x);
    float m = abs(mod(a+u_time*.5, 3.14*2.)-3.14)/3.6;
    float f = radius;
    m += noise(st+u_time*0.05)*.5;
    f += (sin(a*5.)*.1 *pow(m,1.));
    return 1.-smoothstep(f,f+0.007,r);
}

float shapeBorderPosition(vec2 st, vec2 pos, float radius, float width) {
    return shapePosition(st, pos,radius)-shapePosition(st, pos, radius-width);
}
void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(1.0) * shapeBorderPosition(st, u_mouse.xy/u_resolution.xy , u_circleDimension.x, .02) ;

    outputColor = vec4( color, 1.0 );
}