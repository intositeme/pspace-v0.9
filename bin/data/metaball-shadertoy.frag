
//https://www.shadertoy.com/view/4sB3zR
#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_mouse;
uniform vec2 u_resolution;

void main(  )
{
	
	vec2 metaballs[4];
	
	metaballs[0] = vec2(cos(u_time / 10.) * 200.0 + 300.0, sin(u_time / 10.) * 150.0 + 200.0);
	metaballs[1] = vec2(cos(u_time / 10.) * 250.0 + 350.0, sin(u_time / 10. * 0.25) * 100.0 + 200.0);
	metaballs[2] = vec2(cos(u_time / 10. * 0.33) * 300.0 + 350.0, sin(u_time / 10. * 1.5) * 150.0 + 200.0);
	
	metaballs[3] = vec2(u_mouse.xy);
	
	float metaball_size[4];
	
	metaball_size[0] = 1000.0;
	metaball_size[1] = 1000.0;
	metaball_size[2] = 1500.0;
	metaball_size[3] = 2000.0;
	
	float dist[4];
	float test;
	
	float temp1, temp2;
	
	temp1 = gl_FragCoord.x - metaballs[0].x;
	temp2 = gl_FragCoord.y - metaballs[0].y;
	dist[0] = sqrt(temp1 * temp1 + temp2 * temp2);
	test = (metaball_size[0] - 700.) / (dist[0] * dist[0]);
	test = 1. - test;
	dist[0] = metaball_size[0] / (dist[0] * dist[0]);
	dist[0] = dist[0] * test;
	
	temp1 = gl_FragCoord.x - metaballs[1].x;
	temp2 = gl_FragCoord.y - metaballs[1].y;
	dist[1] = sqrt(temp1 * temp1 + temp2 * temp2);
	test = (metaball_size[1] - 700.) / (dist[1] * dist[1]);
	test = 1. - test;
	dist[1] = metaball_size[1] / (dist[1] * dist[1]);
	dist[1] = dist[1] * test;
	
	temp1 = gl_FragCoord.x - metaballs[2].x;
	temp2 = gl_FragCoord.y - metaballs[2].y;
	dist[2] = sqrt(temp1 * temp1 + temp2 * temp2);
	test = (metaball_size[2] - 1100.) / (dist[2] * dist[2]);
	test = 1. - test;
	dist[2] = metaball_size[2] / (dist[2] * dist[2]);
	dist[2] = dist[2] * test;
	
	temp1 = gl_FragCoord.x - metaballs[3].x;
	temp2 = gl_FragCoord.y - metaballs[3].y;
	// dist[3] = sqrt(temp1 * temp1 + temp2 * temp2);
	dist[3] = length(gl_FragCoord.xy - metaballs[3].xy);
	test = (metaball_size[3] - 1400.) / (dist[3] * dist[3]);
	// test =  test;
	// dist[3] = smoothstep(dist[3]-0.1, dist[3], 1.) + smoothstep(dist[3], dist[3]+0.1, 1.);
	dist[3] = metaball_size[3] / (dist[3] * dist[3]);
	dist[3] = dist[3] * test;
	// dist[3] = 1.0 - dist[3];
	//dist[3] = smoothstep( dist[3]-0.1, dist[3]+0.1, 0.2 );
	// vec2 _pos = ((gl_FragCoord.xy - pos) / u_resolution.xy) * vec2(u_resolution.x/u_resolution.y,1.)

	float temp3 = dist[0] + dist[1] + dist[2] + dist[3] ;
	clamp(temp3, 0.0, 1.0);
	
	// fragColor = vec4(temp3 * 0.6, temp3 * 0.8, temp3, 1.0);
	gl_FragColor = vec4(temp3, temp3, temp3, 1.0);
	
}