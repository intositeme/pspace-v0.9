
#ifdef GL_ES
precision mediump float;
#endif

uniform float u_id;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
uniform float circ_opacity;
uniform vec2 u_circleDimension;
// out vec4 outputColor;

float random (float val) { 
    return fract(sin(dot(val,
                         178.233))
                 * 143758.5453123);
}

vec2 random2(vec2 st){
    st = vec2( dot(st,vec2(127.1,311.7)),
              dot(st,vec2(269.5,183.3)) );
    return -1.0 + 2.0*fract(sin(st)*43758.5453123);
}

// Value Noise by Inigo Quilez - iq/2013
// https://www.shadertoy.com/view/lsf3WH
float noise(vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    vec2 u = f*f*(3.0-2.0*f);

    return mix( mix( dot( random2(i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ), 
                     dot( random2(i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
                mix( dot( random2(i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ), 
                     dot( random2(i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

float shapePosition(vec2 st, vec2 pos, float radius, float thickness) {
    st = pos-st;
    st.y *= u_resolution.y/u_resolution.x;
    float r = length(st)*2.0;
    float a = atan(st.y, st.x ) + u_time / 10. * random(u_id);
    float m = abs(mod(a+u_time*.5, 3.14*2.)-3.14)/3.6;
    float f = radius;
    m += noise(st+u_time*1.52)* 1.1 ;
    // f += (sin(a * 2.)* .05 * pow(m,1.));
    f += (cos(a * 2.)* .05 * pow(m,2.));
    return 1.-smoothstep(f- thickness,f+thickness,r);
}

float shapeBorderPosition(vec2 st, vec2 pos, float radius, float width, float thickness) {
    return shapePosition(st, pos,radius, thickness)-shapePosition(st, pos, radius-width, thickness);
}
void main() {
	vec2 st = gl_FragCoord.xy/u_resolution.xy;
    //st.y = st.x;
    // vec3 color = vec3(1.0) * shapeBorderPosition(st, u_mouse.xy/u_resolution.xy ,0.2, .02) ;
    float dist = shapeBorderPosition(st, vec2(0.5) ,0.15, .01, 0.057);
    float dist2 = shapeBorderPosition(st, vec2(0.5) ,0.15, .02, 0.007);
	vec3 color = vec3(1.0, 4., 8.) *  dist + vec3(1.) * dist2;

	gl_FragColor = vec4( color , 1.0 );
}