

#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_mouse;
uniform vec2 u_resolution;
float circ_opacity;
// uniform float circ_opacity;
uniform vec2 u_circleDimension;
// out vec4 outputColor;

void main( void ) {
	circ_opacity = 1.0;
    vec2 pos = u_mouse.xy;
  	pos.y = u_resolution.y - pos.y;
  	vec2 testDimensions = vec2 (100., 100.);
	vec2 _pos = ((gl_FragCoord.xy - pos) / u_resolution.xy) * vec2(u_resolution.x/u_resolution.y,1.);
	float f = 0.0;
	f = 0.1+ .2/10./ abs(length(_pos) - testDimensions.x/u_resolution.x - 0.1);
	// outputColor = vec4(vec3(1.0,1.0,1.0) * f * circ_opacity, circ_opacity *1.0);
	gl_FragColor = vec4(vec3(1.0,1.0,3.0) * f * circ_opacity, circ_opacity *1.0);
}