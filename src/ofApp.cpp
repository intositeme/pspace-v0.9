#include "ofApp.h"
#include "ofxBlobsManager.h"
#include "blobCircle.hpp"
#include "math.h"

#include "ofxGui.h"


using namespace ofxCv;
using namespace cv;


//--------------------------------------------------------------
void ofApp::setup(){

    width = ofGetWidth();
    height = ofGetHeight();
    displayArea = width*height;
    ofBackground(0);
    setupProjectionMapping();//must execute this first!
    
    useCamera = false;
    inputWidth = 100;
    inputHeight = 100;
    
    initVideo();
    setupTracker();
    blobManager.init();
    circleManager.mapper = mapper;
    circleManager.setup();

    vidGrabber.initGrabber(width, height,true);
    //
    //  Thank you for all the times that we have had. I hope we'll be able to continue
    //  to work on pSpace together. And that it'll maybe one day bring us back
    //  together again. I LOVE YOU.
    //  This is a truly wonderful project. Regarding pretty, i think you're the most
    //  BEAUTIFUL lady, pretty doesn't really describe it. I think the word i was searching
    //  for was beautiful. having beauty; possessing qualities that give great
    //  pleasure or satisfaction to see, hear, think about, etc.;
    //  delighting the senses or mind. Not just pretty.
    //  
    //
    /*vector< ofVideoDevice > devices = vidGrabber.listDevices();
    for(int i = 0; i < devices.size(); i++) {
        cout << "Device ID: " << devices[i].id << " Device Name: " << devices[i].deviceName << " Hardware Name: " << devices[i].hardwareName << " Is Available: " << devices[i].bAvailable << endl;
    }*/
    
    setupGUI();
    
}
void ofApp::setupProjectionMapping () {
    mapper =  ofPtr<projectionMapper>(new projectionMapper());
    calibrationPoints.resize(4);
    calibrationVisualPoints.resize(4);
    calibrationPoints[0] = ofPoint(0,0);
    calibrationPoints[1] = ofPoint(width,0);
    calibrationPoints[2] = ofPoint(width,height);
    calibrationPoints[3] = ofPoint(0,height);

    mapper->setPoints(calibrationPoints);
}
void ofApp::setupGUI () {
    
    guiHide = true; //hide GUI initially
    gui.setup(); // most of the time you don't need a name
    gui.add(guiSliderBlobSize.setup("blob size", ofVec2f(50, 200), ofVec2f(10, 10), ofVec2f(300, 300) ));
    gui.add(guiSliderThreshold.setup("Threshold", 255, 0, 255));
    gui.add(guiSliderBrightness.setup("Brightness", -225, -255, 255));
    gui.add(guiSliderContrast.setup("Contrast", 3, 0, 128));
    gui.add(guiToggleCalibrate.setup("Calibrate", false)); //Click to activate projection mapping calibration and click 4 points to map view
    gui.add(guiLabelPoint0.setup("Point 1", ofToString( calibrationPoints[0]) )); 
    gui.add(guiLabelPoint1.setup("Point 2", ofToString( calibrationPoints[1]) )); 
    gui.add(guiLabelPoint2.setup("Point 3", ofToString( calibrationPoints[2]) )); 
    gui.add(guiLabelPoint3.setup("Point 4", ofToString( calibrationPoints[3]) )); 

    gui.loadFromFile("settings.xml");
}
void ofApp::onGuiBlobSizeChanged (ofVec2f &guiSliderBlobSize) {

}

void ofApp::initVideo () {
    if (useCamera) {
        //vidGrabber.setGrabber(ofPtr<ofxPS3EyeGrabber>(new ofxPS3EyeGrabber()));
        vidGrabber.setDeviceID(1);
        vidGrabber.initGrabber(ofGetWindowWidth(), ofGetWindowHeight());
    } else {
        movie.load("calibrate.mp4");
        movie.play();
    }
}

void ofApp::setupTracker () {
    contourFinder.setMinAreaRadius(50);
    contourFinder.setMaxAreaRadius(200);
    contourFinder.setAutoThreshold(false);
    contourFinder.setThreshold(255);
    contourFinder.getTracker().setPersistence(15);
    contourFinder.getTracker().setMaximumDistance(64);
    
//    blobsManager.normalizePercentage = 0.7;
//    blobsManager.giveLowestPossibleIDs = true;
//    blobsManager.maxUndetectedTime = 500;
//    blobsManager.minDetectedTime = 2000;
//    blobsManager.debugDrawCandidates = true;
}

//--------------------------------------------------------------
void ofApp::update(){
    
    //cout << "GUI BlobSize X: " << guiSliderBlobSize->x << " Y: " << guiSliderBlobSize->y;
    contourFinder.setMinAreaRadius(guiSliderBlobSize->x);
    contourFinder.setMaxAreaRadius(guiSliderBlobSize->y);
    contourFinder.setThreshold(guiSliderThreshold);


    ofSetWindowTitle(ofToString(ofGetFrameRate()));

    if (useCamera) {
       vidGrabber.update();
       if(vidGrabber.isFrameNew()) {
           
           camMat = toCv(vidGrabber);
           Mat imgL = camMat + Scalar(guiSliderBrightness, guiSliderBrightness, guiSliderBrightness);
           imgL.convertTo(imgL, -1, guiSliderContrast, 0);
//           ofxCv::blur(imgL, 10);
           toOf(imgL, testImg);
           testImg.update();
           
           //find the blobs
           contourFinder.findContours(imgL);
           //pass blobs to blobManger to keep track of
           inputWidth = vidGrabber.getWidth();
           inputHeight = vidGrabber.getHeight();
           blobManager.update (contourFinder);
       }
    } else {
        movie.update();
        if(movie.isFrameNew()) {
            camMat = toCv(movie);
            Mat imgL = camMat + Scalar(guiSliderBrightness, guiSliderBrightness, guiSliderBrightness);
            imgL.convertTo(imgL, -1, guiSliderContrast, 0);
            ofxCv::blur(imgL, 10);
            toOf(imgL, testImg);
            testImg.update();
            
            // blur(imgL, 100);
            // blur(testImg, 100);
            contourFinder.findContours(imgL);
            inputWidth = movie.getWidth();
            inputHeight = movie.getHeight();
            blobManager.update (contourFinder);
        }
    }
   
    circleManager.update (blobManager.blobsManager.blobs, inputWidth, inputHeight);
    updateCircleRadius();
}
void ofApp::updateCircleRadius () {

}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofSetBackgroundAuto(showLabels);
    drawVideo(showLabels);
    //drawCircles();
    ofEnableAlphaBlending();
    circleManager.draw();
    ofEnableAlphaBlending();
    drawDebug();

    if (!guiHide) {
        gui.draw();


        if(guiToggleCalibrate) {
            ofPushMatrix();
            for (int i = 0; i < calibrationPoints.size(); i++) {

                calibrationVisualPoints[i].setColor(ofColor(255,0,0));
                calibrationVisualPoints[i].clear();
                calibrationVisualPoints[i].moveTo(calibrationPoints[i]);
                calibrationVisualPoints[i].circle(calibrationPoints[i], 10);
                calibrationVisualPoints[i].draw();
            }
            ofPopMatrix();
            //calibrationVisualPoints.draw();
        }
    }
    
    //imshow("New Image", camMat);
}

void ofApp::drawVideo (bool _show) {
    
    if (useCamera) {
        if(_show) {
            ofSetColor(0);
            ofEnableAlphaBlending();
            ofSetColor(255,255,255,120);
//            vidGrabber.draw(0, 0);
            testImg.draw(0, 0);
            ofDisableAlphaBlending();
        }
    } else {
        if(_show) {
            ofSetColor(0);
            ofEnableAlphaBlending();
            ofSetColor(255,255,255,120);
            //movie.draw(0, 0);
            testImg.draw(0, 0);
            contourFinder.draw();
            ofDisableAlphaBlending();
        }
    }
}


//--------------------------------------------------------------

void ofApp::drawCircles () {
    blobCircles.clear();
    ofxBlobsManager	_blobsManager = blobManager.blobsManager;
    for(int i = 0; i < _blobsManager.blobs.size(); i++) {
        blobCircles.push_back(blobCircle());
        blobCircles[i].setup(_blobsManager.blobs[i].centroid, _blobsManager.blobs[i].id, _blobsManager.blobs[i].iniDetectedTime);
        blobCircles[i].draw();
    }
    if (blobCircles.size()>0) {
        ofSetHexColor(0xffffff);
        char reportStr[1024];
        sprintf(reportStr, "blob opacity: %f, fps: %f", blobCircles[0].opacity, ofGetFrameRate());
        ofDrawBitmapString(reportStr, 20, 600);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    blobManager.keyPressed(key);
    if(key == 'c'){
        guiHide = !guiHide;
    }
    else if(key == 's'){
        gui.saveToFile("settings.xml");
    }
    else if(key == 'l'){
        gui.loadFromFile("settings.xml");
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    //save points when calibrating

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    // cout << "mouse Clicked \n" << guiToggleCalibrate;
    if (guiToggleCalibrate) {
        calibrationPoints[calibrationIndex] = ofPoint(x,y);

        if (calibrationIndex ==3) {
            //update projectionMapping settings after 4th point is set
            mapper->setPoints(calibrationPoints);
        }

        calibrationIndex = (calibrationIndex+1)%4;

        guiLabelPoint0 = ofToString(calibrationPoints[0]);
        guiLabelPoint1 = ofToString(calibrationPoints[1]);
        guiLabelPoint2 = ofToString(calibrationPoints[2]);
        guiLabelPoint3 = ofToString(calibrationPoints[3]);
    }
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::drawDebug () {
    ofSetHexColor(0xffffff);
    char reportStr[1024];
    sprintf(reportStr, "fps: %f", ofGetFrameRate());
    ofDrawBitmapString(reportStr, 20, 600);
}
