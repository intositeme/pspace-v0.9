//
//  projectionMapper.hpp
//  pSpace
//
//  Created by Lim Si Ping on 31/12/15.
//
//

#ifndef projectionMapper_hpp
#define projectionMapper_hpp

#include "ofMain.h"

class projectionMapper {
public:
	void setPoints (vector<ofPoint> tempPoints);
	vector<ofPoint> points;
	ofPoint getTranslated (ofPoint point);
private:
	vector<float> systemPoints;
	void getSystem ();
};

#endif /* projectionMapper_hpp */
