//
//  projectionMapper.cpp
//  pSpace
//
//  Created by Lim Si Ping on 31/12/15.
//
//

#include "projectionMapper.hpp"

void projectionMapper::setPoints (vector<ofPoint> tempPoints){
    points = tempPoints;
    getSystem();
    //cout << "setPoints! " << ofToString(points) << "\n";
}

ofPoint projectionMapper::getTranslated (ofPoint point) {
	float a = systemPoints[0];
    float b = systemPoints[1];
    float c = systemPoints[2];
    float d = systemPoints[3];
    float i = systemPoints[4];
    float f = systemPoints[5];
    float g = systemPoints[6];
    float h = systemPoints[7];
    float v = (0-c*g*point.y+f*g*point.x-a*f+a*point.y+c*d-d*point.x) / (0-a*h*point.y+b*g*point.y+d*h*point.x-g*i*point.x+a*i-b*d);
    float u = (c*h*point.y-f*h*point.x+b*f-b*point.y-c*i+i*point.x) / (0-a*h*point.y+b*g*point.y+d*h*point.x-g*i*point.x+a*i-b*d);

    //cout << "getTranslated: "  << ofToString(point) << "\n" << ofToString(ofPoint(u,v)) << "\n";
    return ofPoint(u,v);
}

void projectionMapper::getSystem () {
	vector<ofPoint> P = points;
	systemPoints.clear();
	systemPoints.resize(8);

	float sx = (P[0].x-P[1].x)+(P[2].x-P[3].x);
	float sy = (P[0].y-P[1].y)+(P[2].y-P[3].y);

	float dx1 = P[1].x-P[2].x;
    float dx2 = P[3].x-P[2].x;
    float dy1 = P[1].y-P[2].y;
    float dy2 = P[3].y-P[2].y;
 
    float z = (dx1*dy2)-(dy1*dx2);
    float g = ((sx*dy2)-(sy*dx2))/z;
    float h = ((sy*dx1)-(sx*dy1))/z;

    systemPoints[0]=P[1].x-P[0].x+g*P[1].x;
    systemPoints[1]=P[3].x-P[0].x+h*P[3].x;
    systemPoints[2]=P[0].x;
    systemPoints[3]=P[1].y-P[0].y+g*P[1].y;
    systemPoints[4]=P[3].y-P[0].y+h*P[3].y;
    systemPoints[5]=P[0].y;
    systemPoints[6]=g;
    systemPoints[7]=h;

    //cout << "getSystem! "  << ofToString(systemPoints[0]) << "\n";
}